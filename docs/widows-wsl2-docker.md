# 安裝 Docker Desktop (Windows)

- [安裝 Docker Desktop (Windows)](#安裝-docker-desktop-windows)
  - [安裝 WSL 2](#安裝-wsl-2)
  - [安裝 Docker Desktop](#安裝-docker-desktop)

## 安裝 WSL 2
> 安裝過程中會需要重啟電腦，請依系統指示進行。

1. 「控制台」→「程式集」→「開啟或關閉 Windows 功能」，並開啟 **Windows 子系統 Linux 版** 與 **Windows Hypervisor 平台** 功能
   ![](../images/enable-features.png)
2. 以**系統管理員**身份執行 Windows PowerShell
   ![](../images/powershell.png)
3. 安裝 WSL 2
   * **方法一：** [快速安裝](https://docs.microsoft.com/zh-tw/windows/wsl/install)
     > 必須是 Windows 10 版本 2004 和更新版本執行， (組建 19041 和更新版本) 或 Windows 11

        此命令會啟用必要的選擇性元件、下載最新的 Linux 核心、將 WSL 2 設定為預設值，並預設為您安裝 Linux 發行版本：
        ```sh
        wsl --install
        ```
        若執行上述指令，但看到 WSL 解說文字而非安裝畫面，則表示系統曾經安裝過 WSL。請改由下方的方式安裝 Linux 發行版本：
        ```sh
        # 查看 Linux 發行版本
        wsl --list --online

        # 選擇安裝版本
        wsl --install -d <DistroName>
        # e.g.,
        wsl --install -d Ubuntu:18.04

        # 備註：Ubuntu 18.04 已結束支援，若安裝 Ubuntu，請選擇安裝最新的 LTS 版本 Ubuntu 22.04
        ```
        ![](../images/install-wsl.png)
   * **方法二：** [針對較舊版本的手動安裝步驟](https://docs.microsoft.com/zh-tw/windows/wsl/install-manual)
4. 安裝之後，從搜尋輸入安裝的 Linux 版本並點選
   ![](../images/ubuntu.png)
5. 第一次啟動時需要設定 Linux 使用者資訊
   ![](../images/linux-user.png)
   > 請記得所設定的 Linux 使用者資訊，日後您可能需要用到。

## 安裝 Docker Desktop
1. 下載 Docker Desktop（https://docs.docker.com/desktop/windows/install/)
2. 點選下載的 `Docker Desktop Installer.exe` 並依照指示進行安裝
3. 安裝之後，啟動 Docker Desktop。第一次啟動時，會詢問相關設定，請勾選使用 WSL2
   ![](../images/use-wsl2.png)
   > 若初始階段忘了勾選，後續仍可依[參考文件](https://docs.microsoft.com/zh-tw/windows/wsl/tutorials/wsl-containers#install-docker-desktop)中的步驟 3. 與 4. 進行。

   Docker Desktop 介面
   ![](../images/docker-desktop.png)
4. 後續的 Docker 指令操作皆在 Windows 子系統 Linux 中執行。請執行以下指令查看 Docker 版本：
   ```
   docker version
   ```
   ![](../images/docker-version.png)

5. 如需要找尋 WSL 的文件位置，可在子系統 Linux 執行以下指令找尋：
   ```sh
   explorer.exe .
   ```
   ![](../images/explorer.png)