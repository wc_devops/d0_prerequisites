# prerequisites
完成帳號申請：
- [ ] 1. [GitLab](https://gitlab.com/users/sign_up) [用於 Git Repository]

在您的操作環境完成以下工具安裝：
- [ ] 2. [Git](#安裝-git)
- [ ] 3. [Docker](#安裝-docker)


## 安裝 Git
請根據作業系統選擇[安裝方式](https://git-scm.com/book/zh-tw/v2/%E9%96%8B%E5%A7%8B-Git-%E5%AE%89%E8%A3%9D%E6%95%99%E5%AD%B8)

## 安裝 Docker
1. 請根據作業系統選擇安裝方式
    - https://docs.docker.com/engine/install/
    > Windows: 由於 Windows 安裝 Docker 的步驟較為繁瑣，因此若您的操作環境為 Windows，建議您參考我們所提供的[安裝文件](./docs/widows-wsl2-docker.md)進行安裝

    > macOS: 目前的 Docker Desktop 版本僅支援 macOS 11 以上的版本。若您的 macOS 版本較舊，建議升級您的 OS，或是下載[先前版本](https://docs.docker.com/desktop/previous-versions/archive-mac/)
2. 執行以下指令，以確保 Docker 成功安裝
    ```sh
    $ docker version
    ```
    - 若顯示版本訊息即表示安裝成功，且當前使用者有權限可以執行 docker；
    - 若遇到以下錯誤訊息，請依據[解決 Docker 權限問題](#解決-docker-權限問題)進行設定：
        ```sh
        docker: Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Post http://%2Fvar%2Frun%2Fdocker.sock/v1.35/containers/create: dial unix /var/run/docker.sock: connect: permission denied. See 'docker run --help'.
        ```
### 解決 Docker 權限問題
1. 建立 `docker` 群組（群組可能已存在，請忽略提醒訊息）
   ```sh
   $ sudo groupadd docker
   ```
2. 將當前 `$USER` 加入 docker 群組
    ```sh
    $ sudo usermod -aG docker $USER
    ```
    > 執行完成後，登出該使用者，再重新登入
3. 再次執行以下指令
   ```sh
   $ docker version
   ```
   > 若顯示版本訊息即表示當前使用者已有權限可以執行 docker